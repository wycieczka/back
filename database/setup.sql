DROP TABLE IF EXISTS "images" CASCADE;
DROP TABLE IF EXISTS "trips" CASCADE;
DROP TABLE IF EXISTS "users" CASCADE;
DROP TABLE IF EXISTS "reservations" CASCADE;

CREATE TABLE IF NOT EXISTS "trips" (
  "id" SERIAL PRIMARY KEY,
  "capacity" INTEGER NOT NULL,
  "name" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  "price" INTEGER NOT NULL
);
CREATE TABLE IF NOT EXISTS "images" (
  "path" TEXT NOT NULL,
  "trip_id" INTEGER REFERENCES trips(id) NOT NULL
);

CREATE TABLE IF NOT EXISTS "users" (
  "id" SERIAL PRIMARY KEY,
  "password" TEXT NOT NULL,

  "email" TEXT NOT NULL,
  "first_name" TEXT NOT NULL,
  "family_name" TEXT NOT NULL,
  "phone_number" TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "reservations" (
  "id" SERIAL PRIMARY KEY,
  "ticket_count" INTEGER NOT NULL,

  "email" TEXT NOT NULL,
  "first_name" TEXT NOT NULL,
  "family_name" TEXT NOT NULL,
  "phone_number" TEXT NOT NULL,

  "trip_id" INTEGER REFERENCES trips(id) NOT NULL,
  "user_id" INTEGER REFERENCES users(id)
);

INSERT INTO "trips" VALUES (DEFAULT, 10, 'Morza', 'Mórz jest wiele, więc i opis może być nieco dłuższy niż poprzednio. Atrakcji też może być więcej.', 2000);
INSERT INTO "trips" VALUES (DEFAULT, 30, 'Góry', 'Wycieczka do ciekawych gór.', 3000);
INSERT INTO "trips" VALUES (DEFAULT, 8, 'Miasto', 'Wycieczka do ciekawego miasta.', 2500);

INSERT INTO "users" VALUES (DEFAULT, 'haslo', 'ktos@mail.com', 'Ktostam', 'xBlabla', '1212121');

INSERT INTO "reservations" VALUES (DEFAULT, 2, 'abc@mail.com', 'Olafek', 'Siwinski', '123456789', 2);
INSERT INTO "reservations" VALUES (DEFAULT, 3, 'def@mail.com', 'Larcia', 'Citko', '987654321', 3);
