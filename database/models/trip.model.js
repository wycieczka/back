import { DataTypes  } from 'sequelize';
export default (db) => {
  const Trip = db.define('Trip', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    capacity: {
      type: DataTypes.INTEGER,
    },
    name: {
      type: DataTypes.TEXT,
    },
    description: {
      type: DataTypes.TEXT,
    },
    price: {
      type: DataTypes.INTEGER,
    }
  }, {
    tableName: 'trips',
    timestamps: false
  });


  return Trip;
};