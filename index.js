import path from 'path';
import express from 'express';
import session from 'express-session';
import cors from 'cors';
import db from './database';
import routes from './routes';

const PORT = 8080;

try {
  await db.authenticate();
  await db.sync({ alter: true });
} catch (e) {
  console.log(e);
  process.exit(1);
}

const app = express();
app.use(cors({
  credentials: true
}));
// app.set('trust proxy', 1);
app.use(session({
  secret: '^.^',
  resave: false,
  saveUninitialized: false,
  cookie: { httpOnly: true }
}));
app.use(express.json());
app.set('db', db);
routes(app);

app.listen(PORT);