import express from 'express';
import { body, param, validationResult } from 'express-validator';
import { Sequelize } from 'sequelize';
const router = express.Router();

router.get('/:id', async (req, res) => {
  try {
    const db = req.app.get('db');
    const { id } = req.params;
    const reservation = await db.models.Reservation.findByPk(id);
    return res.status(200).json(reservation);
  } catch (e) {
    console.log(e);
    return res.status(500).end();
  }
});
router.post('/', 
  body('ticket_count')
  .isInt()
  .withMessage(''),
  body('email')
  .isEmail()
  .withMessage(''),
  body('first_name')
  .isLength({ min: 3, max: 32 })
  .withMessage(''),
  body('family_name')
  .isLength({ min: 3, max: 32 })
  .withMessage(''),
  body('phone_number')
  .exists()
  .withMessage(''),
  async (req, res) => {
    const db = req.app.get('db');
    const transaction = await db.transaction();
    try {
      console.log(req.body);
      const { ticket_count, email, first_name, family_name, phone_number, trip_id, user_id } = req.body;
      const trip = await db.models.Trip.findByPk(trip_id, { transaction });
      if (!trip) {
        console.log('asd', trip_id);
        await transaction.rollback();
        return res.status(400).end();
      }
      const count = await db.models.Reservation.sum('ticket_count', { where: { trip_id }}, { transaction }); 
      console.log();
      if (count+Number(ticket_count) > trip.capacity) {
        await transaction.rollback();
        return res.status(400).end();
      }
      const reservation = await db.models.Reservation.create({ ticket_count, email, first_name, family_name, phone_number, trip_id }, { transaction });
      await transaction.commit();
      return res.status(200).json(reservation);
    } catch (e) {
      console.log(e);
      await transaction.rollback();
      return res.status(500).end();
    }
});

export default router;