import { DataTypes  } from "sequelize";
export default (db) => {
  const User = db.define("User", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    email: {
      type: DataTypes.TEXT,
    },
    first_name: {
      type: DataTypes.TEXT,
    },
    family_name: {
      type: DataTypes.TEXT,
    },
    phone_number: {
      type: DataTypes.TEXT,
    },
    password: {
      type: DataTypes.TEXT,
    },
  }, {
    tableName: 'users',
    timestamps: false
  });

  return User;
};