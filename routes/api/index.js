import express from 'express';
const router = express.Router();

import trips from './trips';
import reservations from './reservations';
import user from './user';

router.use('/trips', trips);
router.use('/reservations', reservations);
router.use('/user', user);































// import bcrypt from 'bcrypt';
// import session from 'express-session';
// import { body, param, validationResult } from 'express-validator';




// const SALT_ROUNDS = 16;

// router.post('/register',
//   body('first_name')
//     .isLength({ min: 3, max: 32 })
//     .withMessage(''),
//   body('family_name')
//     .isLength({ min: 3, max: 32 })
//     .withMessage(''),
//   body('email')
//     .isEmail()
//     .withMessage('xd'),
//   body('password')
//     .isLength({ min: 3 })
//     .withMessage(''),
// async (req, res) => {
//   const db = req.app.get('db');
//   const { userId } = req.session;
//   if (userId) {
//     let user = null;
//     try {
//       user = await db.models.User.findByPk(userId);
//     } catch (e) {
//       console.log(e);
//       return res.status(500);
//     }
//     if (user) {
//       return res.redirect('/');
//     }
//     try {
//       await (new Promise((resolve, reject) => { 
//         req.session.destroy((err) => {
//           if (err) {
//             return reject(err);
//           }
//           resolve()
//         });
//       }));
//     } catch (e) {
//       console.log(e);
//       return res.status(500);
//     }
//   }
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     return res.status(400).json({ errors: errors.mapped() });
//   }
//   const { first_name, family_name, email, password } = req.body;

//   let user = null;
//   try {
//     user = await db.models.User.findOne({ where: { email }});
//   } catch (e) {
//     console.log(e);
//     return res.status(500);
//   }
//   if (user) {
//     return res.status(400).json({ errors: { 'email': 'Email already in use' } });
//   }

//   try {
//     const hashed = await bcrypt.hash(password, SALT_ROUNDS);
//     user = await db.models.User.create({ first_name, family_name, email, password: hashed });
//   } catch (e) {
//     console.log(e);
//     return res.status(500);
//   }

//   req.session.userId = user.id;
//   return res.status(200).end(user.email);
// });
// router.post('/login',
//   body('email')
//     .isEmail()
//     .withMessage(''),
//   body('password')
//     .exists()
//     .withMessage(''),
// async (req, res) => {
//   const db = req.app.get('db');

//   const { userId } = req.session;
//   if (userId) {
//     let user = null;
//     try {
//       user = await db.models.User.findByPk(userId);
//     } catch (e) {
//       console.log(e);
//       return res.status(500);
//     }
//     if (user) {
//       return res.result(400);
//     }
//     try {
//       await (new Promise((resolve, reject) => { 
//         req.session.destroy((err) => {
//           if (err) {
//             return reject(err);
//           }
//           resolve()
//         });
//       }));
//     } catch (e) {
//       console.log(e);
//       return res.status(500);
//     }
//   }

//   try { 
//     const user = await db.models.User.findOne({ where: { email }});
//     if (!user) {
//       return res.status(401);
//     }
//     const result = await bcrypt.compare(password, user.password);
//     if (!result) {
//       return res.status(401);
//     }

//     req.session.userId = user.id;
//     return res.status(200);
//   } catch (e) {
//     console.log(e);
//     return res.status(500);
//   }
// });
// router.get('/wycieczka/:id?',
//   param('id')
//   .isInt(),
//   async (req, res) => {
//     try {
//       const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       return res.status(400).end();
//     }
//     const db = req.app.get('db');
//     const { id } = req.params;
//     const result = id ? await db.models.Trip.findByPk(id) : await db.models.Trip.findAll();
//     return res.status(200).json(result);
//   } catch (e) {
//     console.log(e);
//     res.end(500);
//   }
// });

export default router;