import bcrypt from 'bcrypt';
import express from 'express';
import session from 'express-session';
import { body, param, validationResult } from 'express-validator';
const router = express.Router();



const SALT_ROUNDS = 16;

router.post('/register',
  body('first_name')
    .isLength({ min: 3, max: 32 })
    .withMessage(''),
  body('family_name')
    .isLength({ min: 3, max: 32 })
    .withMessage(''),
  body('email')
    .isEmail()
    .withMessage('xd'),
  body('phone_number')
    .isLength({ min: 3 })
    .withMessage(''),
  body('password')
    .isLength({ min: 3 })
    .withMessage(''),
async (req, res) => {
  console.log("user register hej");
  const db = req.app.get('db');
  const { userId } = req.session;
  if (userId) {
    let user = null;
    try {
      user = await db.models.User.findByPk(userId);
    } catch (e) {
      console.log(e);
      return res.status(500);
    }
    if (user) {
      return res.redirect('/');
    }
    try {
      await (new Promise((resolve, reject) => { 
        req.session.destroy((err) => {
          if (err) {
            return reject(err);
          }
          resolve()
        });
      }));
    } catch (e) {
      console.log(e);
      return res.status(500);
    }
  }
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log("errory som");
    return res.status(400).json({ errors: errors.mapped() });
  }
  const { first_name, family_name, email, phone_number, password } = req.body;

  let user = null;
  try {
    user = await db.models.User.findOne({ where: { email }});
  } catch (e) {
    console.log(e);
    return res.status(500);
  }
  if (user) {
    return res.status(400).json({ errors: { 'email': 'Email already in use' } });
  }

  try {
    const hashed = await bcrypt.hash(password, SALT_ROUNDS);
    user = await db.models.User.create({ first_name, family_name, email, phone_number, password: hashed });
  } catch (e) {
    console.log(e);
    return res.status(500);
  }

  req.session.userId = user.id;
  return res.status(200).end(user.email);
});
router.post('/login',
  body('email')
    .isEmail()
    .withMessage(''),
  body('password')
    .exists()
    .withMessage(''),
async (req, res) => {
  console.log("user login hej");
  const db = req.app.get('db');

  const { userId } = req.session;
  if (userId) {
    let user = null;
    try {
      user = await db.models.User.findByPk(userId);
    } catch (e) {
      console.log(e);
      return res.status(500);
    }
    if (user) {
      return res.result(400);
    }
    try {
      await (new Promise((resolve, reject) => { 
        req.session.destroy((err) => {
          if (err) {
            return reject(err);
          }
          resolve()
        });
      }));
    } catch (e) {
      console.log(e);
      return res.status(500);
    }
  }

  const { email, password } = req.body;

  try { 
    const user = await db.models.User.findOne({ where: { email }});
    if (!user) {
      return res.status(401).end();
    }
    const result = await bcrypt.compare(password, user.password);
    if (!result) {
      console.log("zle haslo");
      return res.status(402).end();
    }

    console.log("dobre haslo");
    req.session.userId = user.id;
    return res.status(200).end();
  } catch (e) {
    console.log(e);
    return res.status(500);
  }
});

export default router;