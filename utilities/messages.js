
var Errors = {
    empty_field: "This field shouldn't be empty",
    first_name: "Incorrect first name",
    family_name: "Incorrect family name",
    phone_number: "Incorrect phone number",
    no_free_spots: "There are not enough free spots for this trip"
};