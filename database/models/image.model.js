import { DataTypes  } from 'sequelize';
export default (db) => {
  const Image = db.define('Image', {
    path: {
      type: DataTypes.TEXT
    },
    trip_id: {
      type: DataTypes.INTEGER
    }
  }, {
    tableName: 'images',
    timestamps: false
  });

  return Image;
};