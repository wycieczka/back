import { Sequelize } from 'sequelize';
import { define } from './models';

const db = new Sequelize('www', 'postgres', 'postgres', {
  dialect: 'postgres'
});

define(db);

export default db;