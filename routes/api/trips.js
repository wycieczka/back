import express from 'express';
import { Sequelize, DatabaseError } from 'sequelize';
const router = express.Router();

router.get('/:id?', async (req, res) => {
  console.log('?');
  try {
    const db = req.app.get('db');
    const { id } = req.params;
    if (id) {
      let trip = {};
      try {
        trip = (await db.models.Trip.findByPk(id, { include: db.models.Image })) ?? {};
      } catch (e) {
        if (e instanceof DatabaseError) {
          trip = {};
        } else {
          throw e;
        }
      }
      return res.status(200).json(trip);
    }
    let trips = [];
    try {
      trips = await db.models.Trip.findAll({ include: db.models.Image });
    } catch (e) {
      if (e instanceof DatabaseError) {
        trips = [];
      } else {
        throw e;
      }
    }
    res.status(200).json(trips);
  } catch (e) {
    console.log(e);
    res.status(500).end();
  }
});

export default router;