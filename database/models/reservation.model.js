import { DataTypes  } from 'sequelize';
export default (db) => {
  const Reservation = db.define('Reservation', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    ticket_count: {
      type: DataTypes.INTEGER,
    },
    email: {
      type: DataTypes.TEXT,
    },
    first_name: {
      type: DataTypes.TEXT,
    },
    family_name: {
      type: DataTypes.TEXT,
    },
    phone_number: {
      type: DataTypes.TEXT
    },
    trip_id: {
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER
    }
  }, {
    tableName: 'reservations',
    timestamps: false
  });

  return Reservation;
};