import path from 'path';
import express from 'express';
const router = express.Router();

const FRONT_BUILD_PATH = path.resolve('../front/build/index.html');

router.get('/', (req, res) => {
  res.sendFile(FRONT_BUILD_PATH);
});

export default router;