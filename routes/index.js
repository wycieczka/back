import path from 'path';
import express from 'express';
import document from './document';
import api from './api';

const STATIC_PATH = path.resolve('../front/build');

export const define = (app) => {
  // redirect tailing slashes
  app.use((req, res, next) => {
    const test = /\?[^]*\//.test(req.url);
    if (req.url.substr(-1) === '/' && req.url.length > 1 && !test)
      res.redirect(301, req.url.slice(0, -1));
    else
      next();
  });

  app.use(express.static(STATIC_PATH));
  app.use('/', document);
  app.use('/api', api);
};
export default define;